import React, { useState } from "react";
import "../../css/uploadform.css";
// import {uploadImage} from "../../services/uploadservice"

const Axios = require("axios");
require("dotenv").config();

const CLOUD_NAME = process.env.REACT_APP_CLOUD_NAME;
const preset = process.env.REACT_APP_PRESET_UNSIGN;
const api = `https://api.cloudinary.com/v1_1/${CLOUD_NAME}/upload`;

function App() {
  const [fileInputState, setFileInputState] = useState("");
  const [prevFile, setPrevFile] = useState();
  const [selectedFile, setSelectedFile] = useState("");
  const [uploadedProgress, setUploadProgress] = useState(0);

  const handleFileInputChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      previewFile(file);
    }
  };

  const previewFile = (file) => {
    const fr = new FileReader();
    fr.readAsDataURL(file);
    fr.onloadend = () => {
      setPrevFile(fr.result);
    };
    setSelectedFile(file);
    setUploadProgress(0);
  };

  const handleSubmitFile = (e) => {
    console.log("submit");
    e.preventDefault();
    if (!selectedFile) return;
    let formData = new FormData();
    let percent = 0;
    formData.append("file", selectedFile);
    formData.append("upload_preset", preset);

    const option = {
      onUploadProgress: (ProgressEvent) => {
        const { loaded, total } = ProgressEvent;
        percent = Math.floor((loaded * 100) / total);
        setUploadProgress(percent);
        console.log(`loaded ${loaded}kb of total ${total}|${percent}`);
      },
    };
    Axios.post(api, formData, option).then((res) => console.log(res));
  };

  return (
    <div className="container">
      <h1 className="title">Upload</h1>
      <form onSubmit={handleSubmitFile} className="form">
        <input
          className="fileInput"
          type="file"
          name="image"
          onChange={handleFileInputChange}
          value={fileInputState.name}
        />
        <button className="btn-submit" type="submit">
          Submit
        </button>
      </form>
      <progress
        className="progressBar"
        min="0"
        max="100"
        value={uploadedProgress}
      />
      {prevFile && <img className="preview" src={prevFile} alt="chosen one" />}
    </div>
  );
}

export default App;
