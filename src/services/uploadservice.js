const Axios = require("axios");
require('dotenv').config();

const CLOUD_NAME=process.env.REACT_APP_CLOUD_NAME;
const preset=process.env.REACT_APP_PRESET_UNSIGN;
const api=`https://api.cloudinary.com/v1_1/${CLOUD_NAME}/upload`;


  export function uploadImage(file){
    let formData = new FormData();
    let percent=0;
    formData.append("file", file);
    formData.append("upload_preset", preset);

    const option={
      onUploadProgress:(ProgressEvent)=>{
        const{loaded,total}=ProgressEvent;
        percent=Math.floor(loaded*100/total);
        console.log(`loaded ${loaded}kb of total ${total}|${percent}`);
        
      }
    }
    Axios.post(api, formData,option).then(res=>console.log(res));
  };